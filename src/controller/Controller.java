package controller;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices(  ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-";
		
		manager.loadServices( serviceFile);
	}
	
	public static void loadjson()
	{
		manager.loadjson();
	
	}

		
	
	
}
