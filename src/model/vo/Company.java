package model.vo;

public class Company implements Comparable <Company>{

	private String nombre;
	private int numTaxis;
	private int numServicios;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumTaxis() {
		return numTaxis;
	}

	public void setNumTaxis(int numTaxis) {
		this.numTaxis = numTaxis;
	}

	public int getNumServicios() {
		return numServicios;
	}

	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	
	 public Company(String pnombre) {
		nombre=pnombre;
		numTaxis=1;
		numServicios=1;
	}
	

	@Override
	public int compareTo(Company arg0) {
		
		return nombre.compareTo(arg0.getNombre());
	}
	
	public String toString()
	{
		return  String.format("Compa�ia %s Taxis %d Servicios %d ", nombre , numTaxis,numServicios);
	}
	
}
