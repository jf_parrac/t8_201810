package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {



	private Date fFinal;
	
	private Date fInicial;
	
	private String trip_id;
	
	private String taxi_id;
	
	private double trip_miles;
	
	private int seconds;
	
	private int finArea;
	
	private int startArea;
	
	
	public Service(Date trip_end_timestamp, Date trip_start_timestamp, String trip_id, String taxi_id, double trip_miles,
			int trip_seconds, double trip_total,int dropoff_community_area,int pickup_community_area) {
		
		this.fFinal = trip_end_timestamp;
		this.fInicial = trip_start_timestamp;
		this.trip_id = trip_id;
		this.taxi_id = taxi_id;
		this.trip_miles = trip_miles;
		this.seconds = trip_seconds;
		this.trip_total = trip_total;
		this.finArea=dropoff_community_area;
		this.startArea=pickup_community_area;
		
	}



	public Date getfFinal() {
		return fFinal;
	}



	public Date getfInicial() {
		return fInicial;
	}



	public int getFinArea() {
		return finArea;
	}



	public int getStartArea() {
		return startArea;
	}



	private double trip_total;
	





	public String getTrip_id() {
		return trip_id;
	}



	public String getTaxi_id() {
		return taxi_id;
	}



	public double getTrip_miles() {
		return trip_miles;
	}



	public double getSeconds() {
		return seconds;
	}



	public int getTrip_seconds() {
		return seconds;
	}



	public double getTrip_total() {
		return trip_total;
	}



	@Override
	/**
	 * Compara los servicios por fecha a la que inician
	 */
	public int compareTo(Service arg0) {
		try {
		return arg0.fInicial.compareTo(this.fInicial);
		}
		catch (Exception e) {
			
			return -1;
		}
	}
	
	
	public int compareTo(String tripid)
	{
		return tripid.compareTo(trip_id);
	}
	
	public String toString()
	{
		return "Servicio: "+trip_id+"|| duracion: "+seconds+" Zona inicio: "+startArea+" Distancia recorrida: "+trip_miles;
	}
}
