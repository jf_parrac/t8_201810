package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public interface IMinPQ<Key> {

	/**
	 * Returns true if this priority queue is empty.
	 *
	 * @return {@code true} if this priority queue is empty;
	 *         {@code false} otherwise
	 */
	boolean isEmpty();

	/**
	 * Returns the number of keys on this priority queue.
	 *
	 * @return the number of keys on this priority queue
	 */
	int size();

	/**
	 * Returns a smallest key on this priority queue.
	 *
	 * @return a smallest key on this priority queue
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	Key min();

	/**
	 * Adds a new key to this priority queue.
	 *
	 * @param  x the key to add to this priority queue
	 */
	void insert(Key x);

	/**
	 * Removes and returns a smallest key on this priority queue.
	 *
	 * @return a smallest key on this priority queue
	 * @throws NoSuchElementException if this priority queue is empty
	 */
	Key delMin();

	/**
	 * Returns an iterator that iterates over the keys on this priority queue
	 * in ascending order.
	 * <p>
	 * The iterator doesn't implement {@code remove()} since it's optional.
	 *
	 * @return an iterator that iterates over the keys in ascending order
	 */
	Iterator<Key> iterator();

}