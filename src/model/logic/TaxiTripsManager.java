package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import api.ITaxiTripsManager;
import model.data_structures.ArcoYaExisteException;
import model.data_structures.GrafoDirigido;
import model.data_structures.IArco;
import model.data_structures.IVertice;
import model.data_structures.Lista;
import model.data_structures.VerticeNoExisteException;
import model.data_structures.VerticeYaExisteException;
import model.vo.ClassFactory;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
public class TaxiTripsManager implements ITaxiTripsManager {

	public static final String[] pepeVi�uela= {"./data/taxi-trips-wrvz-psew-subset-small.json","./data/taxi-trips-wrvz-psew-subset-medium.json","./data/taxi-trips-wrvz-psew-subset-large.json"};


	private GrafoDirigido<String,verticeProyecto, arcoProyecto> graph;
	private Double dx;

	private int a=0;
	public void loadServices(String serviceFile) {


		graph = new GrafoDirigido<>();
		dx= 100.0;
		//	graph= new GrafoDirigido<>();


		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
		try
		{
			for(int i=0;i<3;i++)
			{
				a=i;
				ClassFactory[] temp = gson.fromJson(new FileReader(pepeVi�uela[i]) ,ClassFactory[].class);
				dx=100.0;
				GrafofificacionxD(temp);


				System.out.println("==========");
				System.out.println("=escribio");
				System.out.println("==========")

				;
				graph = new GrafoDirigido<>();
				dx=70.0;
				System.out.println("==========");
				System.out.println("=escribio");
				System.out.println("==========");


				GrafofificacionxD(temp);
				graph = new GrafoDirigido<>();
				dx=50.0;
				System.out.println("==========");
				System.out.println("=escribio");
				System.out.println("==========");


				GrafofificacionxD(temp);
				graph = new GrafoDirigido<>();
				dx=25.0;
				System.out.println("==========");
				System.out.println("=escribio");
				System.out.println("==========");


				GrafofificacionxD(temp);
				System.out.println("==========");
				System.out.println("=escribio");
				System.out.println("==========\n\n");



				System.err.println("cambio de file");
			}



		}
		catch (Exception e) {
			// TODO: handle exception
		}

		System.out.println("  ==============================================================");
		System.out.println("||Inside loadServices with File:" + serviceFile);
		System.out.println("|| Dx:"+dx);
		System.out.println("  ==============================================================");
		System.out.println("||Vertices en el grafo: "+graph.darVertices().darLongitud());
		System.out.println("  ==============================================================");
		System.out.println("||arcos en grafo: "+graph.darNArcos());
		System.out.println("  ==============================================================");





	}

	
	private void saveJson(String name)
	{	
				File temp=new File("./jsons/"+name+"-vertex.json");
				File temp1=new File("./jsons/"+name+"-edges.json");
				try { 
					
					
					Writer writer = new FileWriter("./jsons/"+name+"-vertex.json"); 
					Writer writer1 = new FileWriter("./jsons/"+name+"-edges.json");
//					-keepattributes Signature
					Gson gson = new GsonBuilder().create();
					
					Gson gson2 = new GsonBuilder().create();
					
					
					
					Lista<verticeProyecto> vertices= graph.darVertices();
				
				
						gson.toJson(vertices,writer);
					
				
					
					Lista<arcoProyecto> arcos= graph.darArcos();
					
						gson2.toJson(arcos, writer1);						
					
					writer.close();
					writer1.close();
				}
		
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			
		
		


	}


	public void loadjson()
	{
				
				try {
					Gson gson = new Gson();
					Gson gson2 = new Gson();
					FileReader fr = new FileReader("./jsons/small-vertex.json");
					FileReader fr1 = new FileReader("./jsons/small-edges.json");
					
					
					
					Lista<verticeProyecto> vertices =	gson.fromJson(fr, (new TypeToken<Lista<verticeProyecto>>() {}).getType());
					
				
//					Type listType = new TypeToken<ArrayList<verticeProyecto>>(){}.getType();
//					Lista<verticeProyecto> vertices = new Gson().fromJson(fr, listType);
					
					Lista<arcoProyecto> arcos =	gson2.fromJson(fr1, (new TypeToken<Lista<arcoProyecto>>() {}).getType());
				
					double[] test;
					graph= new GrafoDirigido<>();
					for(int i=0;i<vertices.darLongitud();i++)
					{
						try {
							verticeProyecto p=vertices.darElemento(i);
							graph.agregarVertice(p);
							test=p.id;
						} catch (VerticeYaExisteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					System.out.println("Verdadero numero vertices: "+graph.darVertices().darLongitud());
					
					for(int i=0;i<arcos.darLongitud();i++)
					{
						arcoProyecto temp= arcos.darElemento(i);
						try {
							graph.agregarArco(temp.origen, temp.destino, temp);
							
						} catch (VerticeNoExisteException | ArcoYaExisteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					
					}
					
					
					
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				//		


	}

	private void log(String serviceFile)
	{
		saveJson((serviceFile.substring(35).substring(0,serviceFile.substring(35).length()-5)));
		File log = new File("./Leame.txt");
		if(!log.exists())
		{
			try {
				log.createNewFile();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {

			FileWriter fileWriter = new FileWriter("./Leame.txt", true);  //appends to file


			fileWriter.write("||==============================================================\n");
			fileWriter.write("||Inside loadServices with File:" + serviceFile.substring(35)+"\n");
			fileWriter.write("|| Dx:"+dx+"\n");
			fileWriter.write("||==============================================================\n");
			fileWriter.write("||Vertices en el grafo: "+graph.darVertices().darLongitud()+"\n");
			fileWriter.write("||==============================================================\n");
			fileWriter.write("||arcos en grafo: "+graph.darNArcos()+"\n");
			fileWriter.write("||==============================================================\n");

			fileWriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



	}

	private double getDistance (double lat1, double lon1, double lat2, double lon2)
	{

		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1);
		Double lonDistance =  Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos( Math.toRadians(lat1))
		* Math.cos( Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

	public class verticeProyecto implements IVertice<String>
	{
		private static final long serialVersionUID = -9213919773490764421L;
		Lista<String> servicios;
		double[] id;
		@Override
		public String darId() {
			return id[0]+":"+id[1];
		}

		public Lista<String> darServicios()
		{
			return servicios;
		}

		public void AddServce(String p)
		{
			servicios.agregar(p);
		}
		public verticeProyecto(double[] ds){
			this.id=ds;
			servicios= new Lista<>();
		}
	}

	public class arcoProyecto implements IArco
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 5172517899656285712L;

		/***
		 * <b>1:  </b>  Distancia Recorrida<br>
		 * <b>2:  </b>  Valor del servicio<br> 
		 *  <b>3:  </b>  Tiempo del servicio<br>
		 *  <b>4:  </b>   Peajes<br>
		 */
		double[] data= {0,0,0,0};

		/***
		 * <b>1:  </b>  Distancia Recorrida<br>
		 * <b>2:  </b>  Valor del servicio<br> 
		 *  <b>3:  </b>  Tiempo del servicio<br>
		 *  <b>4:  </b>   Peajes<br>
		 */
		double[] cumule= {0,0,0,0};

		String origen;

		String destino;
		int numserv;
		public arcoProyecto() {
			numserv=0;
		}
		public arcoProyecto(String darId, String darId2) {
			origen=darId;
			destino=darId2;
			// TODO Auto-generated constructor stub
		}
		public arcoProyecto(String darId, String darId2,int a) {
			origen=darId;
			destino=darId2;
			numserv=a;
			// TODO Auto-generated constructor stub
		}
		public void agregar(double[] newData)
		{
			for(int i=0;i<4;i++)
			{
				cumule[i]+=newData[i];
			}
			numserv++;
			average();
		}

		private void average()
		{
			for(int i=0;i<4;i++)
			{
				data[i]=cumule[i]/numserv;
			}

		}


		@Override
		public double darPeso() {
			// TODO Auto-generated method stub
			return data[0];
		}

		/***
		 * <b>1:  </b>  Distancia Recorrida<br>
		 * <b>2:  </b>  Valor del servicio<br> 
		 *  <b>3:  </b>  Tiempo del servicio<br>
		 *  <b>4:  </b>   Peajes<br>
		 */
		@Override
		public double darPeso(int i)
		{
			return data[i];
		}

		public double[] darPesoTotal()
		{
			return data;
		}



	}
	private void GrafofificacionxD(ClassFactory[] temp)
	{
		try {
			for(ClassFactory dataLoaded:temp)
			{
				boolean existeInicio=false;
				double tempmin=Double.MAX_VALUE;
				verticeProyecto vi=null;

				Lista<verticeProyecto> pinchecosasiniterador=graph.darVertices();
				for(int i=0;i<graph.darVertices().darLongitud();i++)
				{
					verticeProyecto t=  pinchecosasiniterador.darElemento(i);
					double a=getDistance(t.id[0], t.id[1], dataLoaded.getPickup_centroid_latitude(), dataLoaded.getPickup_centroid_longitude());
					if(a<=dx)
					{

						if(a<tempmin)
						{
							vi=t;
							existeInicio=true;
						}

					}
				}
				if(!existeInicio)
				{
					vi=new verticeProyecto(dataLoaded.getPickCoordinates());
					graph.agregarVertice(vi);
					pinchecosasiniterador=graph.darVertices();
				}
				
				vi.AddServce(dataLoaded.getTrip_id());


				//vertice final
				boolean existeFin=false;
				double tempmin2=Double.MAX_VALUE;
				verticeProyecto vf=null;
				for(int i=0;i<graph.darVertices().darLongitud();i++)
				{

					verticeProyecto t=  pinchecosasiniterador.darElemento(i);
					//
					double a=getDistance(t.id[0], t.id[1], dataLoaded.getDropoff_centroid_latitude(), dataLoaded.getDropoff_centroid_longitude());
					if(a<=dx)
					{

						if(a<tempmin2)
						{
							vf=t;
							existeFin=true;
						}

					}
				}
				if(!existeFin)
				{
					vf=new verticeProyecto(dataLoaded.getdropCoordinates());
					graph.agregarVertice(vf);
					vf=graph.darVertice(vf.darId());

				}
				vf.AddServce(dataLoaded.getTrip_id());
				//
				if(!graph.existeArco(vi.darId(), vf.darId()))
				{
					graph.agregarArco(vi.darId(), vf.darId(), new arcoProyecto(vi.darId(), vf.darId()));

				}
				/***
				 * <b>1:  </b>  Distancia Recorrida<br>
				 * <b>2:  </b>  Valor del servicio<br> 
				 *  <b>3:  </b>  Tiempo del servicio<br>
				 *  <b>4:  </b>   Peajes<br>
				 */
				double[] newData= {dataLoaded.getTrip_miles(),dataLoaded.getTrip_total(),(double)dataLoaded.getTrip_seconds(),(double)dataLoaded.getFare()};
				((arcoProyecto)graph.darArco(vi.darId(), vf.darId())).agregar(newData); 

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		log(pepeVi�uela[a]);
	}




}
